Source: python-gd
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Julián Moreno Patiño <julian@debian.org>
Build-Depends: debhelper-compat (= 12),
 dh-python,
 libfreetype6-dev,
 libgd-dev,
 libgif-dev,
 libjpeg-dev,
 libpng-dev,
 libx11-dev,
 python-all-dbg (>= 2.6.6-3~),
 python-all-dev (>= 2.6.6-3~),
 zlib1g-dev
Standards-Version: 3.9.8
Homepage: https://newcenturycomputers.net/projects/gdmodule.html
Vcs-Git: https://salsa.debian.org/python-team/packages/python-gd.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-gd

Package: python-gd
Architecture: any
Depends: fonts-freefont-ttf, ${misc:Depends}, ${python:Depends}, ${shlibs:Depends}
Suggests: python-gd-dbg
Description: Python module wrapper for libgd
 Provides PNG, JPEG, GIF and XPM creation and manipulation routines
 through the libgd library.
 .
 Also allows use of TrueType fonts in images created.

Package: python-gd-dbg
Section: debug
Architecture: any
Depends: python-gd (= ${binary:Version}),
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends}
Description: Python module wrapper for libgd (debug extension)
 Provides PNG, JPEG, and XPM creation and manipulation routines
 through the libgd library.
 .
 This package contains the extension built for the Python debug interpreter.
